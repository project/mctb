
Menu Child Teaser Block
=======================

Display TEASERS of the CHILD ITEMS of the current page MENU ITEM in a BLOCK.

When this block is positioned under the content (eg in the 'content' region)
and you are viewing a page that is in the menu and has some child items
(nodes only) then the teasers of these pages will be collected and displayed. 

This then acts as an automatic table of contents for any section you are
looking at, ensuring the content items within the section are readily
displayed.

This produces expected behaviour in some architectures, and reduces the need
for introductory (parent, topic or index) pages to enumerate content in the
deeper pages.

This is a pretty small module, with a single purpose. To change the display
of the teasers, this should be done in theme functions or CCK field display
settings. Some new theme callbacks are defined that you can override if you
like.

TO INSTALL
----------
Once the module is enabled, you need to visit the blocks administration
page, and place the new block "Menu Child Teasers" on the page, in a
'content' or 'under content' type region. Configure visibility as usual.


@author dman http://coders.co.nz 